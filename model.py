from IPython import get_ipython
get_ipython().magic('reset -sf')

import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
from sklearn import model_selection
import datetime
import os
from PIL import Image


N_FEATURES = 784
IMG_SIDE = 28
N_CLASSES = 3

train_percent = 0.5
test_percent = 0.5

X_train = 0;
X_test = 0;
Y_train = 0;
Y_test = 0;

def model_datasets():
    print('Loading all images...')
    cactus = np.load('cactus.npy')
    face = np.load('face.npy')
    house = np.load('house.npy')

    M = np.shape(cactus)[0] + np.shape(face)[0] + np.shape(house)[0]
    X = np.zeros([M, N_FEATURES])
    Y = np.zeros([M, N_CLASSES])

    index = 0
    Xcount = 0
    for i in [cactus, face, house]:
        print('Starting')
        m_klass = np.shape(i)[0]
        klass = index * np.ones(m_klass, dtype=np.int)
        yi = np.eye(N_CLASSES)[klass]

        print('Assigning X, Y')
        X[Xcount:Xcount + m_klass, :] = i
        Y[Xcount:Xcount + m_klass, :] = yi
        index = index + 1
        Xcount = Xcount + m_klass

    print('Normalizing...')
    X = normalize(X)
    return model_selection.train_test_split(X, Y, test_size=0.3, random_state=42)

def load_model_datasets():
    global X_train, X_test, Y_train, Y_test
    X_train, X_test, Y_train, Y_test = model_datasets();

def normalize(a):
    return a/np.max(a)

def show_random_images():
    for i in range(30):
        image_1d = X_train[np.random.randint(np.shape(X_train)[0]), :]
        image = np.reshape(image_1d, (-1, IMG_SIDE))

        im = Image.fromarray(image)
        im.save("sample-image-{}.tiff".format(i))

def get_saver():
    saver = tf.train.Saver()
    # saver_path = './models/' + datetime.datetime.now().strftime ("%Y%m%d%H%M%S") + '/model'
    saver_path = './models/board/' + datetime.datetime.now().strftime ("%Y-%m-%d-%H-%M-%S") + "/"
    if not os.path.exists(saver_path):
        os.makedirs(saver_path)

    return saver, saver_path

def train():
    with tf.Graph().as_default() as graph:
        print('Reading datasets...')
        X_train, X_test, Y_train, Y_test = model_datasets();
        print('Done')

        # Case a) One input layer, one output layer
        x = tf.placeholder(tf.float32, [None, N_FEATURES], name="x")
        W = tf.Variable(tf.random_uniform([N_FEATURES, N_CLASSES], -1., 1.), name="w1")
        bias = tf.Variable(tf.random_uniform([N_CLASSES], -1., 1.), name="b1")

        y = tf.add(tf.matmul(x, W), bias)
        y_ = tf.placeholder(tf.float32, [None, N_CLASSES], name="y")

        # Case b) One input layer, 1 hidden layer, 1 output layer
        # x = tf.placeholder(tf.float32, [None, N_FEATURES], name="Input")
        # W_1 = tf.Variable(tf.random_uniform([N_FEATURES, N_FEATURES/10], -1., 1.), name="Weights1")
        # W_2 = tf.Variable(tf.random_uniform([N_FEATURES/10, N_CLASSES], -1., 1.), name="Weights2")
        # bias_1 = tf.Variable(tf.random_uniform([N_FEATURES/10], -1., 1.), name="Bias1")
        # bias_2 = tf.Variable(tf.random_uniform([N_CLASSES], -1., 1.), name="Bias2")
        # y_ = tf.placeholder(tf.float32, [None, N_CLASSES], name="Labels")
        # h1 = tf.add(tf.matmul(x, W_1), bias_1)
        # y = tf.add(tf.matmul(h1, W_2), bias_2)

        cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels = y_, logits = y))

        train_step = tf.train.GradientDescentOptimizer(0.5).minimize(cross_entropy)

        correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(y_, 1))
        accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

        session = tf.InteractiveSession()
        tf.global_variables_initializer().run()

        saver, saver_path = get_saver()

        # Log summary of all values:
        # Case a)
        tf.summary.scalar("cross_entropy", cross_entropy)
        tf.summary.scalar("accuracy", accuracy)
        tf.summary.histogram("Weights", W)
        tf.summary.histogram("Bias", bias)

        # Case b)
        # tf.summary.scalar("cross_entropy", cross_entropy)
        # tf.summary.scalar("accuracy", accuracy)
        # tf.summary.histogram("Weights_1", W_1)
        # tf.summary.histogram("Weights_2", W_2)
        # tf.summary.histogram("Bias_1", bias_1)
        # tf.summary.histogram("Bias_2", bias_2)

        merged_summary_op = tf.summary.merge_all()
        summary_writer = tf.summary.FileWriter(saver_path, graph=graph)

        print('Training starting')
        for step in range(1000):
            print(str(step) + ' Train step')
            run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
            run_metadata = tf.RunMetadata()

            _, loss, summary = session.run([train_step, cross_entropy, merged_summary_op], feed_dict={x: X_train, y_: Y_train}, options = run_options, run_metadata = run_metadata)

            print('Calculating loss and accuracy...')
            print('Loss: ' + str(loss))
            accuracy_step = session.run([accuracy], feed_dict={x: X_test, y_: Y_test})
            print('Accuracy: ' + str(accuracy_step))

            print('Saving state...')
            saver.save(session, saver_path, global_step = step)

            print('Saving summary...')
            summary_writer.add_run_metadata(run_metadata = run_metadata, tag = "step" + str(step), global_step = step)
            summary_writer.add_summary(summary, step)

def load_and_process_image(imname, black_threshold = 128):
    im = Image.open(imname)
    # im = im.resize((IMG_SIDE, IMG_SIDE), Image.BICUBIC) # Resize image
    im = im.convert('L') # Convert to grayscale
    imdata = np.asarray(im).copy() # Convert to matrix

    imdata_bw = imdata
    imdata_bw[imdata < black_threshold] = 0.
    imdata_bw[imdata >= black_threshold] = 255.
    imdata_bw = 255. - imdata_bw

    imbw = Image.fromarray(imdata_bw)
    imbw = imbw.resize((IMG_SIDE, IMG_SIDE), Image.BICUBIC)

    imdata_bw = np.asarray(imbw).copy()
    # Normalize values
    imdata_bw = normalize(imdata_bw)
    imbw = Image.fromarray(imdata_bw)
    imbw.save("{}-processed.tiff".format(imname))

    return imdata_bw

def name_for_index(index = -1):
    if (index == 0):
        return "Cactus"
    elif (index == 1) :
        return "Face"
    elif (index == 2):
        return "House"


def classify_image(imname, black_threshold = 128, model_path = './'):
    image = load_and_process_image(imname, black_threshold)

    tf.reset_default_graph()
    x = tf.placeholder(tf.float32, [None, N_FEATURES])
    W = tf.Variable(tf.random_normal([N_FEATURES, N_CLASSES]), name="w1")
    bias = tf.Variable(tf.random_normal([N_CLASSES]), name="b1")

    y = tf.add(tf.matmul(x, W), bias)
    y_ = tf.placeholder(tf.float32, [None, N_CLASSES])

    cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels = y_, logits = y))

    with tf.Session() as sess:
        tf.train.Saver().restore(sess, tf.train.latest_checkpoint(model_path))

        image_array = np.expand_dims(image.flatten(), axis=0)

        prediction = tf.argmax(y, 1)
        certainty = tf.reduce_max(tf.nn.softmax(y))
        other_certainties = tf.nn.softmax(y)

        result, certainty_level, others = sess.run([prediction, certainty, other_certainties], {x: image_array})

        print("This is a " + name_for_index(result))
        print("I'm {}% sure about it".format(certainty_level * 100))
        print("Other predictions were " + str(others) + " (Cactus, face, house)")
